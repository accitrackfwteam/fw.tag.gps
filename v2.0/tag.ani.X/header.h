// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   AcciTag
//
//      UNIT    :   Project header - define all include files
//
//      AUTHOR  :   Nico Bestbier                  
//                  Ph : +27 (0)82 313-0903
//                  E-mail : bes@xsinet.co.za
//
// **************************************************************************

#ifndef __HEADER_H
#define __HEADER_H

#include <htc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFF_LEN    100

#include "G:\Johann\Proj\AcciTrack\FW\Common\serial.h"
#include "types.h"
#include "config.h"
#include "i2c_comms.h"
#include "factory_tests.h"
#include "fifo.h"
#include "G:\Johann\Proj\AcciTrack\FW\Common\common.h"
#include "rf.h"
#include "cpu.h"
#include "a2d.h"
#include "gps.h"

#endif
