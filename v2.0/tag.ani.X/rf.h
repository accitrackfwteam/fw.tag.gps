// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   AcciTrack
//
//      UNIT    :   rf.h
//
//      AUTHOR  :   Nico Bestbier                  
//                  Ph : +27 (0)82 313-0903
//                  E-mail : bes@xsinet.co.za
//
// **************************************************************************

#ifndef __RF_H
#define __RF_H

#ifdef __RF_C
 #define RFLOCN
#else
 #define RFLOCN extern
#endif


//C1100 Register Definitions
#define IOCFG2    0x00  //GDO2 output pin configuration.
#define IOCFG1    0x01  //GDO1 output pin configuration.
#define IOCFG0    0x02  //GDO0 output pin configuration. Refer to SmartRF� Studio User Manual for detailed pseudo register explanation.
#define FIFOTHR   0x03  //RX FIFO and TX FIFO thresholds
#define SYNC1     0x04  //Sync word, high byte
#define SYNC0     0x05  //Sync word, low byte
#define PKTLEN    0x06  //Packet length.
#define PKTCTRL1  0x07  //Packet automation control.
#define PKTCTRL0  0x08  //Packet automation control.
#define ADDR      0x09  //Device address.
#define CHANNR    0x0A  //Channel number.
#define FSCTRL1   0x0B  //Frequency synthesizer control.
#define FSCTRL0   0x0C  //Frequency synthesizer control.
#define FREQ2     0x0D  //Frequency control word, high byte.
#define FREQ1     0x0E  //Frequency control word, middle byte.
#define FREQ0     0x0F  //Frequency control word, low byte.
#define MDMCFG4   0x10  //Modem configuration.
#define MDMCFG3   0x11  //Modem configuration.
#define MDMCFG2   0x12  //Modem configuration.
#define MDMCFG1   0x13  //Modem configuration.
#define MDMCFG0   0x14  //Modem configuration.
#define DEVIATN   0x15  //Modem deviation setting (when FSK modulation is enabled).
#define MCSM2     0x16  //Main Radio Control State Machine configuration
#define MCSM1     0x17  //Main Radio Control State Machine configuration
#define MCSM0     0x18  //Main Radio Control State Machine configuration.
#define FOCCFG    0x19  //Frequency Offset Compensation Configuration.
#define BSCFG     0x1A  //Bit synchronization Configuration.
#define AGCCTRL2  0x1B  //AGC control.
#define AGCCTRL1  0x1C  //AGC control.
#define AGCCTRL0  0x1D  //AGC control.
#define WOREVT1   0x1E  //High byte event0 timeout
#define WOREVT0   0x1F  //Low byte event0 timeout
#define WORCTRL   0x20  //Wake On Radio control
#define FREND1    0x21  //Front end RX configuration.
#define FREND0    0x22  //Front end RX configuration.
#define FSCAL3    0x23  //Frequency synthesizer calibration.
#define FSCAL2    0x24  //Frequency synthesizer calibration.
#define FSCAL1    0x25  //Frequency synthesizer calibration.
#define FSCAL0    0x26  //Frequency synthesizer calibration.
#define RCCTRL1   0x27  //RC oscillator configuration
#define RCCTRL0   0x28  //RC oscillator configuration
#define FSTEST    0x29  //Frequency synthesizer calibration.
#define PTEST     0x2A  //Production test
#define AGCTEST   0x2B  //AGC test
#define TEST2     0x2C  //Various test settings.
#define TEST1     0x2D  //Various test settings.
#define TEST0     0x2E  //Various test settings.
#define PARTNUM	  0x30	//Get part number
#define VERSION   0x31  //Get chip version

#define SRES      0x30  //Reset chip.
#define SFSTXON   0x31  //Enable and calibrate frequency synthesizer (if MCSM0.FS_AUTOCAL=1). If in RX (with CCA): Go to a wait state where only the synthesizer is running (for quick RX / TX turnaround).
#define SXOFF     0x32  //Turn off crystal oscillator.
#define SCAL      0x33  //Calibrate frequency synthesizer and turn it off (enables quick start). SCAL can be strobed from IDLE mode without setting manual calibration mode (MCSM0.FS_AUTOCAL=0).
#define SRX       0x34  //Enable RX. Perform calibration first if coming from IDLE and MCSM0.FS_AUTOCAL=1.
#define STX       0x35  //In IDLE state: Enable TX. Perform calibration first if MCSM0.FS_AUTOCAL=1. If in RX state and CCA is enabled: Only go to TX if channel is clear.
#define SIDLE     0x36  //Exit RX / TX, turn off frequency synthesizer and exit Wake-On-Radio mode if applicable.
#define SAFC      0x37  //Perform AFC adjustment of the frequency synthesizer.
#define SWOR      0x38  //Start automatic RX polling sequence (Wake-on-Radio).
#define SPWD      0x39  //Enter power down mode when CSn goes high.
#define SFRX      0x3A  //Flush the RX FIFO buffer.
#define SFTX      0x3B  //Flush the TX FIFO buffer.
#define SWORRST   0x3C  //Reset real time clock.
#define SNOP	  0x3D  //Reset real time clock.

//Status Registers (read-only)
#define RXBYTES   0x3B  //Read number of RX bytes received
#define PATABLE   0x3E
#define FIFO      0x3F

#define CCA_ALWAYS  0
#define CCA_RSSI    1
#define CCA_PACKET  2
#define CCA_BOTH    3

#define RXOFF_IDLE    0
#define RXOFF_FSTXON  1
#define RXOFF_TX      2
#define RXOFF_RX      3

#define TXOFF_IDLE    0
#define TXOFF_FSTXON  1
#define TXOFF_TX      2
#define TXOFF_RX      3


#define RX_CYCLE_TIME	10	/* mili seconds */
//#define WOR_EVENT0 		(26000000.0 / 750000.0)*RX_CYCLE_TIME
#define PACKET_FILTER           0x77
#define PANIC_TRIES		1
#define PCM_TRIES		1
//#define TX_POWER		0xC0 // 12dBm => CC1101,
//#define TX_POWER		0x86 // 5dBm => CC1101,
//#define TX_POWER		0x50 // 0dBm => CC1101,
//#define TX_POWER		0x54 // -3dBm => CC1101,
#define TX_POWER		0x37 // -6dBm => CC1101
//#define TX_POWER		0x26 // -10dBm => CC1101


const unsigned char rf_settings[36][2] = 
{
 {FSCTRL1,  0x08},       //Frequency synthesizer control.
 {FSCTRL0,  0x00},       //Frequency synthesizer control.
 {FREQ2,    0x21},       //Frequency control word, high byte.
 {FREQ1,    0x62},       //Frequency control word, middle byte.
 {FREQ0,    0xF4},       //Frequency control word, low byte.
 {MDMCFG4,  0x7B},       //7B Modem configuration - BANDWIDTH: 232kHz 
 {MDMCFG3,  0x83},       //83 Modem configuration - DATA RATE: 76.8kBaud
 {MDMCFG2,  0x03},       //03 Modem configuration - FORMAT: 2-FSK
 {MDMCFG1,  0xA0},       //A0 Modem configuration - CHANNEL SPACING: 25kHz, Enable FEC
 {MDMCFG0,  0x00},       //00 Modem configuration - LOWEST CHANNEL SPACING: 0
 {CHANNR,   0x00},       //Channel number.
 {DEVIATN,  0x42},       //Modem deviation setting - DEVIATION: 32kHz
 {FREND1,   0xB6},       //Front end RX configuration.
 {FREND0,   0x10},       //Front end RX configuration.
 {MCSM1,    0x20},       //Main Radio Control State Machine configuration.
 {MCSM0,    0x18},       //Main Radio Control State Machine configuration.
 {FOCCFG,   0x1D},       //Frequency Offset Compensation Configuration.
 {BSCFG,    0x1C},       //Bit synchronization Configuration.
 {AGCCTRL2, 0xC7},       //AGC control.
 {AGCCTRL1, 0x00},       //AGC control.
 {AGCCTRL0, 0xB2},       //AGC control.
 {FSCAL3,   0xEA},       //Frequency synthesizer calibration.
 {FSCAL2,   0x2A},       //Frequency synthesizer calibration.
 {FSCAL1,   0x00},       //Frequency synthesizer calibration.
 {FSCAL0,   0x1F},       //Frequency synthesizer calibration.
 {FSTEST,   0x59},       //Frequency synthesizer calibration.
 {TEST2,    0x81},       //Various test settings.
 {TEST1,    0x35},       //Various test settings.
 {TEST0,    0x09},       //Various test settings.
 {IOCFG2,   0x25},       //*GDO2 output pin configuration.
 {IOCFG0,   0x06},       //GDO0 output pin configuration. Refer to SmartRF� Studio User Manual for detailed pseudo register explanation.
 {PKTCTRL1, 0x4F},       //*Packet automation control - Whitening OFF
 {PKTCTRL0, 0x05},       //Packet automation control.
 {ADDR,     0xFF},       //Device address.
 {PKTLEN,   0xFF},	 //Packet length.
 {FIFOTHR,  0x00}
};


//Local Macros - STROBES
#define POWER_ON_RESET  RF_Strobe(SRES)
#define CRYSTAL_OFF     RF_Strobe(SXOFF)
#define FREQ_SYNTH_ON   RF_Strobe(SFSTXON)
#define FREQ_SYNTH_CAL  RF_Strobe(SCAL)
#define ENABLE_RX       RF_Strobe(SRX)
#define ENABLE_TX       RF_Strobe(STX)
#define IDLE            RF_Strobe(SIDLE);
#define AFC_ADJUST      RF_Strobe(SAFC)
#define START_WOR       RF_Strobe(SWOR)
#define WOR_RESET       RF_Strobe(SWORRST)
#define POWER_DOWN      RF_Strobe(SPWD)
#define FLUSH_RX_FIFO   RF_Strobe(SFRX)
#define FLUSH_TX_FIFO   RF_Strobe(SFTX)
#define CC_STATUS	RF_Strobe(SNOP)


#define WAIT_IDLE(time)		time = 0;	while(CC_STATUS & 0b01110000)		{ WaitMs(1); time++; if(time > 10) { cpu_state.fields.rf_error = TRUE; break; } }
#define WAIT_RX(time)		time = 0;	while(!(CC_STATUS & 0b00010000))	{ WaitMs(1); time++; if(time > 10) { cpu_state.fields.rf_error = TRUE; break; } }
#define WAIT_TX(time)		time = 0;	while(!(CC_STATUS & 0b00100000))	{ WaitMs(1); time++; if(time > 10) { cpu_state.fields.rf_error = TRUE; break; } }
#define WAIT_CALIBRATE(time)	time = 0;	while(!(CC_STATUS & 0b01000000))	{ WaitMs(1); time++; if(time > 10) { cpu_state.fields.rf_error = TRUE; break; } }
#define WAIT_SETTLING(time)	time = 0;	while(!(CC_STATUS & 0b01010000))	{ WaitMs(1); time++; if(time > 10) { cpu_state.fields.rf_error = TRUE; break; } }

#define IS_RX			(CC_STATUS & 0b00010000)
#define IS_IDLE			(!(CC_STATUS & 0b01110000))
#define WAIT_BF 		while(!BF) continue


//WOR_RES = 1
//EVENT1  = 0
//RX_TIME = 6
//Tevent0 = (923.077e-6)*EVENT0
#define SETUP_AND_START_WOR		ConfigWOR(1, 1083, 0, 6, 1, 0)  // Will wake every 1 sec
//#define SETUP_AND_START_WOR		ConfigWOR(1, 5416, 0, 6, 1, 0)	// Will wake every 5 sec

RFLOCN u8 rf_data[BUFF_LEN];

RFLOCN bool InitRF();
RFLOCN void InitSPI();
RFLOCN void ProcessRF_Events();
RFLOCN void ProcessRF_Timing();
RFLOCN void RF_TX_Packet(u8 *data, u8 len, u8 address, u16 preamble_time);
RFLOCN void ConfigWOR(u8 resolution, u16 event0, u8 event1, u8 rx_timeout, u8 qual, u8 term_rssi);
RFLOCN void TransmitCarrier();
RFLOCN void RF_BroadcastHealth();
RFLOCN bool RF_Test(bool reset);

#endif