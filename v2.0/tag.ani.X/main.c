// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   AcciTag
//
//      UNIT    :   Entry point
//
//      AUTHOR  :   Nico Bestbier                  
//                  Ph : +27 (0)82 313-0903
//                  E-mail : bes@xsinet.co.za
//
// **************************************************************************

#include "header.h"


// Program Config Word 1
__CONFIG(FOSC_INTOSC & WDTE_ON & PWRTE_OFF & MCLRE_ON & CP_ON & CPD_OFF & BOREN_OFF & CLKOUTEN_OFF & IESO_OFF & FCMEN_ON);                         

// Program Config Word 2
__CONFIG(WRT_ALL & PLLEN_OFF & STVREN_ON & BORV_LO & LVP_ON);

//#define DO_DEBUG


void main(void)
{    
    //Initialization
    InitCpu();    
    InitA2D();
    InitRF();
    InitNonVolatile();
         
    //Set the accelerometer in async mode
    ACC_AsyncMode();
   
    while (TRUE)
    {
        //Processes
        ProcessTimers();
        ProcessRF_Events();
        ProcessRF_Timing();        

        #if (HAS_GPS == TRUE)
        ProcessGPS();
        #else
        ProcessFactoryTests();
        cpu_state.fields.gps_active = FALSE;
        #endif
                      

        //Micro - Sleep
        #ifdef DO_DEBUG
            WaitMs(128);
        #else
            //Reset Watchdog and go to sleep (will wake-up every ~128ms)
            CLRWDT();
            //PA_OFF;
            //LNA_OFF;
            LNA_ON;

            if (cpu_state.fields.factory_test_mode || cpu_state.fields.gps_active)
            {
                WaitMs(128);
            }
            else SLEEP();
        #endif

        //Process the vibration sensor
        #if HAS_VIBRATION == TRUE
        if (IOCBF0) { IOCBF0 = 0; sensor_data.motion_cnt++; }
        #endif

        //Process the accelerometer
        ACC_Update(FALSE);
    }
}