// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   AcciTag
//
//      UNIT    :   gps.h
//
//      AUTHOR  :   Nico Bestbier
//                  Ph : +27 (0)79 976-3335
//                  E-mail : nico@accitrack.co.za
//
// **************************************************************************

#ifndef __GPS_H
#define __GPS_H

#ifdef __GPS_C
 #define GPSLOCN
#else
 #define GPSLOCN extern
#endif


GPSLOCN bool location_rdy;
GPSLOCN struct_basic_location_data gps_info;
GPSLOCN void InitGPS();
GPSLOCN void ProcessGPS();

#endif