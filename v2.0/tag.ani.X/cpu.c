// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   AcciTag
//
//      UNIT    :   gps.c
//
//      AUTHOR  :   Nico Bestbier
//                  Ph : +27 (0)79 976-3335
//                  E-mail : nico@accitrack.co.za
//
// **************************************************************************

#ifndef __CPU_C
#define __CPU_C

#include "header.h"


// **************************************************************************
//
//  FUNCTION    :   InitCpu
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Initialize the CPU here.
//
//  CHANGES     :   2010/11/19 - Create operation
//
// **************************************************************************
void InitCpu()
{
    u8 i;

    //Disable Global Interrupts
    GIE = 0;

    //Set CPU speed to 8MHz
    IRCF3 = 1;
    IRCF2 = 1;
    IRCF1 = 1;
    IRCF0 = 0;

    //Port defaults
    ADON = 0;				// Disable the A2D module

    //Outputs
    TRISB6 = PORT_OUTPUT; RB6 = 0;              //LED[RB6]
    TRISB5 = PORT_OUTPUT; RB5 = 1;
    TRISB4 = PORT_OUTPUT; RB4 = 1;
    TRISB3 = PORT_OUTPUT; RB3 = 1;
    TRISB2 = PORT_OUTPUT; RB2 = 1;
    TRISB1 = PORT_OUTPUT; RB1 = 1;
    TRISB7 = PORT_OUTPUT; RB7 = 1;

    TRISC5 = PORT_OUTPUT;                       //SDO[RC5]
    TRISC3 = PORT_OUTPUT;                       //SCL[RC3]
    TRISC1 = PORT_OUTPUT; RC1 = 1;              //CC_CSn[RC1]
    TRISC6 = PORT_OUTPUT; RC6 = 0;              //TX[RC6]***
    TRISA7 = PORT_OUTPUT; RA7 = 0;              //GPS_ON[RA7]

    //Inputs
    TRISA2 = PORT_INPUT;  ANSA2 = 1;            //AN2[RA2]
    TRISC4 = PORT_INPUT;                        //SDI[RC4]
    TRISC0 = PORT_INPUT;                        //GDO2[RC0]
    TRISC7 = PORT_INPUT;                        //RX[RC7]

    //Power Amp
    TRISB2 = PORT_OUTPUT; RB2 = 0; ANSB2 = 0;   //HGM
    TRISB1 = PORT_OUTPUT; RB1 = 0; ANSB1 = 0;   //LNA_EN
    TRISB5 = PORT_OUTPUT; RB5 = 0; ANSB5 = 0;   //PA_EN


    //The WDT derives its time base from the 32kHz clock - interval 128 ms
    WDTPS4 = 0;
    WDTPS3 = 0;
    WDTPS2 = 1;
    WDTPS1 = 1;
    WDTPS0 = 1;

    cpu_state.all = 0;
    cpu_timers.tx_health_timer = 0;
    cpu_timers.gps_timeout_timer = TIME_DISABLED;
    cpu_state.fields.factory_test_mode = FALSE;
    location_rdy = FALSE;
    
    /*
    #if GPS_DEBUG == TRUE
    cpu_timers.gps_interval_timer = EXPIRED;
    #else
    cpu_timers.gps_interval_timer = GPS_INTERVAL;
    #endif*/
    
    cpu_timers.gps_interval_timer = EXPIRED;

 
    //Implement port B pin 0 interrupt
    TRISB0 = 1;                         // Set PORTB pin 0 to input
    ANSB0 = 0;                          // Set PORTB pin 0 to Digital input
    IOCBF0 = 0;                         // Clear the Inturrupt Flag
    IOCBP0 = 1;                         // Set PB0 pin as a Interrupt on Change pin. Rising edge INT.
    IOCIE = 0;                          // Disable Interrupt on Change
    
    //Startup indication
    for (i=0; i<10; i++) { LED_TOG; WaitMs(50); }
}


// **************************************************************************
//
//  FUNCTION    :   InitNonVolatile
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Get the non-volatile values from the EEPROM
//
//  CHANGES     :   2013/10/11 - Create operation
//
// **************************************************************************
void InitNonVolatile()
{
    //Get the device serial number
    eeprom_read_str(EADR_SRN, device_id, 11);

    #if TEST_TAG
    strcpy(device_id, TEST_SRN);
    #else
    if ((device_id[0] == 0xFF) || (device_id[0] == 0x00)) strcpy(device_id, "BAD_SRN_PAR");
    #endif

}

// **************************************************************************
//
//  FUNCTION    :   WaitMs
//
//  IN PARA     :   u16 time
//
//  OUT PARA    :   void
//
//  OPERATION   :   Delay function (ms)
//
//  CHANGES     :   2010/03/16 - Create operation
//
// **************************************************************************
u16 ms_i, ms_j;
void WaitMs(u16 time)
{
    //Locals
    ms_i = 0;

    while (ms_i < time)
    {
        ms_j = 0;

        while(ms_j < 150)
        {
            CLRWDT();

            ms_j++;
        }

        ms_i++;
    }
}


// **************************************************************************
//
//  FUNCTION    :   WaitUs
//
//  IN PARA     :   u16 time
//
//  OUT PARA    :   void
//
//  OPERATION   :   Delay function (us)
//
//  CHANGES     :   2010/03/16 - Create operation
//
// **************************************************************************
void WaitUs(u16 time)
{
	for (ms_i=0; ms_i<time; ms_i++) continue;
}


// **************************************************************************
//
//  FUNCTION    :   ProcessTimers()
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Handle the cpu timers
//
//  CHANGES     :   2010/11/21 - Create operation
//
// **************************************************************************
void ProcessTimers()
{
    //Local timers
    if (cpu_timers.tx_health_timer > 0)     cpu_timers.tx_health_timer--;
    if (cpu_timers.motion_timer > 0)        cpu_timers.motion_timer--;
    if (cpu_timers.motion_send_timer > 0)   cpu_timers.motion_send_timer--;
    if (cpu_timers.gps_interval_timer > 0)  cpu_timers.gps_interval_timer--;
    if (cpu_timers.gps_timeout_timer > 0)   cpu_timers.gps_timeout_timer--;
    if (cpu_timers.gps_send_timer > 0)      cpu_timers.gps_send_timer--;
}

#endif

// **************************************************************************
//
//  FUNCTION    :   isr
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Handle all interrupts here
//
//  CHANGES     :   2011/07/05 - Create operation
//
// **************************************************************************
void interrupt isr(void)
{
    u8 this_byte;

    if (RCIF)
    {
        //Read character
        this_byte = RCREG;

        //Route to command interface
        if (!cpu_state.fields.new_cmd_ready)
        {
            if (this_byte != '\n')
            {
                if (this_byte != '\r')
                {
                    if (comms_in_ptr < BUFF_LEN-1)
                    {
                        comms_buff[comms_in_ptr++] = this_byte;
                    }
                }
                else
                {
                    //Commit command
                    comms_in_len = comms_in_ptr;
                    comms_buff[comms_in_ptr++] = 0;
                    comms_in_ptr = 0;
                    cpu_state.fields.new_cmd_ready = TRUE;
                }
            }
        }
    }
}
