// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   AcciTag
//
//      UNIT    :   fifo.c
//
//      AUTHOR  :   Nico Bestbier                  
//                  Ph : +27 (0)82 313-0903
//                  E-mail : bes@xsinet.co.za
//
// **************************************************************************

#ifndef __FIFO_C
#define __FIFO_C

#include "header.h"


// **************************************************************************
//
//  FUNCTION    :   InitStack
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Initialize the stack
//
//  CHANGES     :   2011/08/19 - Create operation
//
// **************************************************************************
void InitStack()
{
	//Locals
	u8 i;

	//Reset entries
	for (i=0; i<STACK_SIZE; i++)
	{
		stack_time[i] = 0;
		stack_array[i] = 0;
	}
}


// **************************************************************************
//
//  FUNCTION    :   stack_push
//
//  IN PARA     :   u8* snr
//
//  OUT PARA    :   void
//
//  OPERATION   :   Push a SNR into an empty slot in the stack.
//
//  CHANGES     :   2011/08/19 - Create operation
//
// **************************************************************************
bool stack_push(u8* snr)
{
	//Locals
	u8 i;

	//Find empty entry
	for (i=0; i<STACK_SIZE; i++)
	{
		//Find an open slot
		if(stack_time[i] == 0)
		{
			stack_time[i] = TEN_SECONDS;
			stack_array[i] = atoi(snr);

			//Found an open slot
			return (TRUE);
		}
	}

	//No Space
	return(FALSE);
}


// **************************************************************************
//
//  FUNCTION    :   stack_exists
//
//  IN PARA     :   u8* snr
//
//  OUT PARA    :   bool
//
//  OPERATION   :   Check if an entry exists in the stack
//
//  CHANGES     :   2011/08/19 - Create operation
//
// **************************************************************************
bool stack_exists(u8* snr)
{
	//Locals
	u8 i;
	u32 snr_real = atoi(snr);

	//Find empty entry
	for (i=0; i<STACK_SIZE; i++)
	{
		//Find an empty slot
		if(stack_time[i] != 0)
		{
			if (stack_array[i] == snr_real) return(TRUE);
		}
	}

	//Not found
	return(FALSE);
}


// **************************************************************************
//
//  FUNCTION    :   stack_process
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Decrement all timers.
//
//  CHANGES     :   2011/08/19 - Create operation
//
// **************************************************************************
void stack_process()
{
	//Locals
	u8 i;

	//Remove old entries
	for (i=0; i<STACK_SIZE; i++)
	{
		//Decrement timers
		if (stack_time[i] > 0) stack_time[i]--;
	}
}


#endif