// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   AcciTrack
//
//      UNIT    :   types.h
//
//      AUTHOR  :   Nico Bestbier                  
//                  Ph : +27 (0)82 313-0903
//                  E-mail : bes@xsinet.co.za
//
// **************************************************************************

typedef unsigned char 	bool;
typedef unsigned char 	u8;
typedef signed char 	s8;
typedef unsigned short 	u16;
typedef signed short	s16;
typedef unsigned long 	u32;
typedef signed long		s32;


#define TRUE	1
#define FALSE	0

#define PORT_INPUT		1
#define PORT_OUTPUT		0
