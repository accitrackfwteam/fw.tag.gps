// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   AcciTrack
//
//      UNIT    :   a2d.h
//
//      AUTHOR  :   Nico Bestbier                  
//                  Ph : +27 (0)82 313-0903
//                  E-mail : bes@xsinet.co.za
//
// **************************************************************************

#ifndef __A2D_H
#define __A2D_H

#ifdef __A2D_C
 #define A2DLOCN
#else
 #define A2DLOCN extern
#endif


A2DLOCN void InitA2D();
A2DLOCN void A2D_BatteryVoltage();
A2DLOCN void A2D_QualifyBattery();
A2DLOCN void A2D_ProcessCharger();


#endif