// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   imet.sense
//
//      UNIT    :   factory_tests.h
//
//      AUTHOR  :   Nico Bestbier
//                  Ph : +27 (0)79 976-3335
//                  E-mail : nico@accitrack.co.za
//
// **************************************************************************

#ifndef __TST_H
#define __TST_H

#ifdef __TST_C
#define TSTLOCN
#else
#define TSTLOCN extern
#endif

#define COMMS_BUFF_LEN  32

#define EADR_SRN        0

#define REPLY_OK                printf("OK\r\n")
#define REPLY_ERROR             printf("ERROR\r\n")
#define REPLY_UNSUPPORTED       printf("ERROR: UNSUPPORTED\r\n")
#define REPLY_BAD_FORMAT        printf("ERROR: BADFORMAT\r\n")

TSTLOCN u16 comms_in_ptr;
TSTLOCN u16 comms_in_len;

#if HAS_GPS == TRUE
TSTLOCN u8 comms_buff[BUFF_LEN];
#else
TSTLOCN u8* comms_buff;
#endif

TSTLOCN void eeprom_read_str(u8 adr, u8* str, u8 len);
TSTLOCN void eeprom_write_str(u8 adr, u8* str, u8 len);

TSTLOCN void InitUart(bool enable_rx);
TSTLOCN void ProcessFactoryTests();
TSTLOCN void ProcessTestCmds();

#endif
