// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   tag.ani
//
//      UNIT    :   factory_tests.c
//
//      AUTHOR  :   Nico Bestbier
//                  Ph : +27 (0)79 976-3335
//                  E-mail : nico@accitrack.co.za
//
// **************************************************************************
#ifndef __TST_C
#define __TST_C

#include "header.h"

//Smallest error @ BAUD = 19200
#define BAUD 		19200
#define FOSC 		8000000L
#define DIVIDER 	((int)(FOSC/(16UL * BAUD) - 1))


// **************************************************************************
//
//  FUNCTION    :   InitFactoryTests
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Initialize the GSM module
//
//  CHANGES     :   2010/03/15 - Create operation
//
// **************************************************************************
void InitUart(bool enable_rx)
{
    //Baud rate
    SPBRGL = DIVIDER;                           //BAUD rate = FOSC/[16 * (SPBRG + 1)]
    BRG16 = 0;

    //Transmitter setup
    CSRC  = 0;					//Ignore
    TX9   = 0;					//Selects 8-bit transmission
    TXEN  = 1;					//Transmit enabled
    SYNC  = 0;					//Asynchronous mode
    BRGH  = 1;					//High Speed
    TX9D  = 0;					//Ninth bit of Transmit Data

    //Receiver setup
    RCIF  = 0;					//Clear interrupt flag
    RCIE  = enable_rx;                          //Enable interrupt
    RX9   = 0;					//Selects 8-bit reception
    SREN  = 0;					//Ignore
    ADDEN = 0;					//Disables address detection
    CREN  = enable_rx;

    SPEN  = 1;					//Serial port enabled

    //Comm Vars
    comms_in_ptr = 0;
    comms_in_len = 0;

    #if HAS_GPS == FALSE
    comms_buff = rf_data;
    #endif
}


// **************************************************************************
//
//  FUNCTION    :   putch
//
//  IN PARA     :   u8 c
//
//  OUT PARA    :   void
//
//  OPERATION   :   Works with printf
//
//  CHANGES     :   2012/06/11 - Create operation
//
// **************************************************************************
void putch(u8 c)
{
    while(!TXIF) continue;

    TXREG = c;
}

void eeprom_write_str(u8 adr, u8* str, u8 len)
{
    //Locals
    u8 i;

    for (i=0; i<len; i++) eeprom_write(adr + i, str[i]);

    //Null terminate
    eeprom_write(adr + len, 0);
}

void eeprom_read_str(u8 adr, u8* str, u8 len)
{
    //Locals
    u8 i = 0;

    for (i=0; i<len; i++)
    {
        str[i] = eeprom_read(adr + i);
    }

    str[len] = 0;
}

void ProcessFactoryTests()
{
    //Do factory tests
    if (cpu_state.fields.factory_test_mode)
    {
        LED_ON;

        //Enable interrupts (we need interrupts for the UART)
        GIE = 1; PEIE = 1;

        //Check for port errors
        if (RCSTAbits.FERR || RCSTAbits.OERR) { SPEN = 0; SPEN = 1; }

        //Do command handling here
        if (cpu_state.fields.new_cmd_ready)
        {
            ProcessTestCmds();

            //Done handling command
            cpu_state.fields.new_cmd_ready = FALSE;
        }

        //Check if cable is disconnected
        if (!IS_RX_HI) cpu_timers.factory_mode_counter++; else cpu_timers.factory_mode_counter = 0;
        
        if (cpu_timers.factory_mode_counter > 100) 
        {
            while(TRUE);
        }
    }
    else if (IS_RX_HI)
    {
        //Initialization
        InitUart(TRUE);
        WaitMs(10);

        //Test mode is now active
        cpu_timers.factory_mode_counter = 0;
        cpu_state.fields.factory_test_mode = TRUE;
    }
}


void ProcessTestCmds()
{
    u16 i;

    //ECHO Command -------------------------------------------------------------
    if (strncmp(comms_buff, "ECH", 3) == 0)
    {
        switch(comms_buff[3])
        {
            case '=':
            {
                printf("ECH: %s\r\n", &comms_buff[4]);

                break;
            }
            case '?':
            {
                REPLY_BAD_FORMAT;

                break;
            }
            default:
            {
                REPLY_BAD_FORMAT;

                break;
            }
        }
    }
    //SRN Command -------------------------------------------------------------
    else if (strncmp(comms_buff, "SRN", 3) == 0)
    {
        switch(comms_buff[3])
        {
            case '=':
            {
                if (strlen(&comms_buff[4]) == 11)
                {
                    eeprom_write_str(EADR_SRN, &comms_buff[4], 11);
                    eeprom_read_str(EADR_SRN, device_id, 11);                    
                    REPLY_OK;
                }
                else
                {
                    REPLY_BAD_FORMAT;
                }

                break;
            }
            case '?':
            {
                printf("SRN: %s\r\n", device_id);

                break;
            }
            default:
            {
                REPLY_BAD_FORMAT;

                break;
            }
        }
    }
    //CARRIER TEST Command -----------------------------------------------------
    else if (strncmp(comms_buff, "CAR", 3) == 0)
    {
        switch(comms_buff[3])
        {
            case '!':
            {
                //Enable power amplifier
                HGM_OFF; PA_ON; WaitMs(2);

                //Carrier Test
                TransmitCarrier();

                //Wait some time
                for (i=0; i<10; i++) { printf("CAR: BUSY[%d]\r\n", i); LED_TOG; WaitMs(500);  }

                //Disable power amplifier
                HGM_OFF; PA_OFF;

                //Finished
                InitRF();

                REPLY_OK;

                break;
            }
            case '?':
            {
                REPLY_BAD_FORMAT;

                break;
            }
            default:
            {
                REPLY_BAD_FORMAT;

                break;
            }
        }
    }
    //VIBRATION TEST Command -----------------------------------------------------
    else if (strncmp(comms_buff, "VIB", 3) == 0)
    {
        switch(comms_buff[3])
        {
            case '=':
            {
                REPLY_BAD_FORMAT;

                break;
            }
            case '?':
            {
                printf("VIB: %d\r\n", sensor_data.motion_cnt);

                break;
            }
            default:
            {
                REPLY_BAD_FORMAT;

                break;
            }
        }
    }
    //ACCELEROMETER TEST Command -----------------------------------------------------
    else if (strncmp(comms_buff, "ACC", 3) == 0)
    {
        switch(comms_buff[3])
        {
            case '=':
            {
                REPLY_BAD_FORMAT;

                break;
            }
            case '?':
            {
                cpu_state.fields.i2c_error = FALSE;
                ACC_AsyncMode();
                WaitMs(100);

                s8 x, y, z;
                ACC_AsyncRead(&x, &y, &z);

                if (!cpu_state.fields.i2c_error)
                {
                    printf("ACC: (%d,%d,%d)\r\n", x, y, z);
                }
                else
                {
                    REPLY_ERROR;
                }

                break;
            }
            default:
            {
                REPLY_BAD_FORMAT;

                break;
            }
        }
    }
    //SPI COMMS TEST Command -----------------------------------------------------
    else if (strncmp(comms_buff, "RFT", 3) == 0)
    {
        switch(comms_buff[3])
        {
            case '=':
            {
                REPLY_BAD_FORMAT;

                break;
            }
            case '?':
            {
                cpu_state.fields.rf_error = FALSE;
                InitRF();

                if (!cpu_state.fields.rf_error)
                {
                    REPLY_OK;
                }
                else
                {
                    REPLY_ERROR;
                }

                break;
            }
            default:
            {
                REPLY_BAD_FORMAT;

                break;
            }
        }
    }
    //SPI COMMS TEST Command -----------------------------------------------------
    else if (strncmp(comms_buff, "RST!", 4) == 0)
    {
        while(TRUE);
    }
    else
    {
        REPLY_UNSUPPORTED;
    }

    comms_buff[0] = 0;
}

#endif
