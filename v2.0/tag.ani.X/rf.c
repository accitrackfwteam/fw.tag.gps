// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   AcciTag
//
//      UNIT    :   rf.c
//
//      AUTHOR  :   Nico Bestbier                  
//                  Ph : +27 (0)82 313-0903
//                  E-mail : bes@xsinet.co.za
//
// **************************************************************************

#ifndef __RF_C
#define __RF_C

#include "header.h"

static u8 rf_packet_index = 0;


// **************************************************************************
//
//  FUNCTION    :   InitSPI
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   : 	Init SPI interface
//
//  CHANGES     :   2010/05/08 - Create operation
//
// **************************************************************************
void InitSPI()
{
    //Setup serial port
    TRISC3 = PORT_OUTPUT; 		   				//SCL[RC3]
    TRISC4 = PORT_INPUT; 						//SDI[RC4]
    TRISC5 = PORT_OUTPUT; 		   				//SDO[RC5]

    //SSPCON: SYNC SERIAL PORT CONTROL REGISTER (SPI MODE)
    WCOL 	= 0;
    SSPOV	= 0;
    SSPEN	= 1;
    CKP         = 0;
    SSPM3	= 0;
    SSPM2	= 0;
    SSPM1	= 0;
    SSPM0	= 0;

    //SSPSTAT: SYNC SERIAL PORT STATUS REGISTER (SPI MODE)
    SMP         = 0;
    CKE 	= 1;
}


// **************************************************************************
//
//  FUNCTION    :   RF_Read
//
//  IN PARA     :   u8 addr, u8 *data, u8 *stat, bool is_strobe
//
//  OUT PARA    :   void
//
//  OPERATION   :   Byte-read from tranceiver
//
//  CHANGES     :   2010/07/03 - Create operation
//
// **************************************************************************
void RF_Read(u8 addr, u8 *data, u8 *stat, bool is_strobe)
{
    //Only A[5]..A[0]
    addr &= 0b00111111;

    //Read bit
    addr |= 0b10000000;

    //Burst bit
    if (is_strobe) addr |= 0b01000000;

    //Chip select low
    CC_CSn_LO;

    //Transmit address
    SSPBUF = addr;

    //Wait until transmit is done
    WAIT_BF;

    //Dummy read
    *stat = SSPBUF;

    //Dummy write
    SSPBUF = 0x00;

    //Wait until transmit is done
    WAIT_BF;

    //Read data
    *data = SSPBUF;

    //Chip select high
    CC_CSn_HI;
}


// **************************************************************************
//
//  FUNCTION    :   RF_BurstRead
//
//  IN PARA     :   u8 *buf, u8 cnt
//
//  OUT PARA    :   void
//
//  OPERATION   :   Burst-read from FIFO
//
//  CHANGES     :   2010/07/03 - Create operation
//
// **************************************************************************
void RF_FIFO_Read(u8 *buf, u8 cnt)
{
    u8 i;

    //Chip select low
    CC_CSn_LO;

    //FIFO address and read+burst bit
    SSPBUF = FIFO | 0b11000000;

    //Wait until transmit is done
    WAIT_BF;

    //Dummy read
    i = SSPBUF;

    for (i = 0; i< cnt; i++)
    {
        //Dummy write
        SSPBUF = 0x00;

        //Wait until transmit is done
        WAIT_BF;

        //Read data
        buf[i] = SSPBUF;
    }

    //Chip select high
    CC_CSn_HI;
}


// **************************************************************************
//
//  FUNCTION    :   RF_Write
//
//  IN PARA     :   u8 addr, u8 data, u8 *stat
//
//  OUT PARA    :   void
//
//  OPERATION   :   Byte-write to the tranceiver
//
//  CHANGES     :   2010/07/03 - Create operation
//
// **************************************************************************
void RF_Write(u8 addr, u8 data, u8 *stat)
{
    //Only A[5]..A[0]
    addr &= 0b00111111;

    //Chip select low
    CC_CSn_LO;

    //Transmit address
    SSPBUF = addr;

    //Wait until transmit is done
    WAIT_BF;

    //Read status byte
    *stat = SSPBUF;

    //Write data
    SSPBUF = data;

    //Wait until transmit is done
    //while(!BF) continue;
    WAIT_BF;

    //Read status byte
    *stat = SSPBUF;

    //Chip select high
    CC_CSn_HI;
}


// **************************************************************************
//
//  FUNCTION    :   RF_Strobe
//
//  IN PARA     :   u8 addr
//
//  OUT PARA    :   u8
//
//  OPERATION   :   Write strobe to the tranceiver
//
//  CHANGES     :   2010/07/03 - Create operation
//
// **************************************************************************
u8 RF_Strobe(u8 addr)
{
    //Locals
    u8 stat;

    //Only A[5]..A[0]
    addr &= 0b00111111;

    //Chip select low
    CC_CSn_LO;

    //Transmit address
    SSPBUF = addr;

    //Wait until transmit is done
    WAIT_BF;

    //Read status byte
    stat = SSPBUF;

    //Chip select high
    CC_CSn_HI;

    return(stat);
}


// **************************************************************************
//
//  FUNCTION    :   RF_Default
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Set RF default configuration
//
//  CHANGES     :   2010/07/03 - Create operation
//
// **************************************************************************
void RF_Default()
{
    //Locals
    u8 i, addr, data, stat;


    //Set default rf settings
    for (i=0; i<36; i++)
    {
            addr = rf_settings[i][0];
            data = rf_settings[i][1];

            RF_Write(addr, data, &stat);
    }

    RF_Write(PATABLE, TX_POWER, &stat);
}


// **************************************************************************
//
//  FUNCTION    :   RF_RxCount
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Read the amount of data received by the tranceiver.
//
//  CHANGES     :   2010/07/03 - Create operation
//
// **************************************************************************
u8 RF_RxCount()
{
    //Locals
    u8 data, stat;

    RF_Read(RXBYTES, &data, &stat, TRUE);

    return(data & 0b01111111);
}


// **************************************************************************
//
//  FUNCTION    :   RF_Test
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Test the radio interface
//
//  CHANGES     :   2010/11/20 - Create operation
//
// **************************************************************************
bool RF_Test(bool reset)
{
    //Locals
    u8 data, stat;

    //Must reset?
    if (reset) { RF_Strobe(SRES); WaitMs(10); }

    //Write and read from address
    RF_Write(ADDR, 0xAE, &stat);
    WaitMs(1);
    RF_Read(ADDR, &data, &stat, TRUE);

    //Result
    return(data == 0xAE);
}


// **************************************************************************
//
//  FUNCTION    :   RF_SetAddress
//
//  IN PARA     :   u8 adr
//
//  OUT PARA    :   void
//
//  OPERATION   :   Set RF address
//
//  CHANGES     :   2010/07/05 - Create operation
//
// **************************************************************************
void RF_SetAddress(u8 adr)
{
    u8 stat;

    RF_Write(ADDR, adr, &stat);
}


// **************************************************************************
//
//  FUNCTION    :   RF_SetOffModes
//
//  IN PARA     :   u8 tx_off, u8 rx_off
//
//  OUT PARA    :   void
//
//  OPERATION   :   Set RX and TX off-modes
//
//  CHANGES     :   2010/11/20 - Create operation
//
// **************************************************************************
void RF_SetOffModes(u8 tx_off, u8 rx_off)
{
    u8 stat, val;

    //CCA = "Unless currently receiving a packet"
    val = 0b00100000;

    val |= tx_off;
    val |= rx_off << 2;

    RF_Write(MCSM1, val, &stat);
}


// **************************************************************************
//
//  FUNCTION    :   RF_TX_Packet
//
//  IN PARA     :   u8 *data, u8 len, u8 address, u16 preamble_time
//
//  OUT PARA    :   void
//
//  OPERATION   :   Transmit data with specified length and address.
//
//  CHANGES     :   2010/07/05 - Create operation
//
// **************************************************************************
void RF_TX_Packet(u8 *data, u8 len, u8 address, u16 preamble_time)
{
    //Locals
    u8 i, t, stat;

    //Setup interface
    InitSPI();

    //Status must be IDLE
    stat = CC_STATUS; if (!IS_IDLE) { IDLE; WAIT_IDLE(t); }

    //Enable power amplifier
    HGM_OFF; PA_ON;
    WaitMs(2);

    //Stay in TX
    RF_SetOffModes(TXOFF_TX, RXOFF_IDLE);

    //Start transmitter
    ENABLE_TX;

    //Send preamble
    if (preamble_time > 0) WaitMs(preamble_time);

    //Write length to FIFO (must add 1 for address field)
    RF_Write(FIFO, len + 1, &stat);

    //Write address to FIFO
    RF_Write(FIFO, address, &stat);

    //Push data to FIFO
    for (i=0; i<len; i++) RF_Write(FIFO, data[i], &stat);

    //Set TX-off to idle
    RF_SetOffModes(TXOFF_IDLE, RXOFF_IDLE);

    //Wait for idle state
    WAIT_IDLE(t);

    //Disable power amplifier
    PA_OFF; HGM_OFF;
}


// **************************************************************************
//
//  FUNCTION    :   ConfigWOR
//
//  IN PARA     :   u8 resolution, u16 event0, u8 event1, u8 rx_timeout, u8 qual, u8 term_rssi
//
//  OUT PARA    :   void
//
//  OPERATION   :   Configure wake on radio
//
//  CHANGES     :   2010/07/05 - Create operation
//
// **************************************************************************
void ConfigWOR(u8 resolution, u16 event0, u8 event1, u8 rx_timeout, u8 qual, u8 term_rssi)
{
    //Locals
    u8 stat, t;

    //Resolution : 0-3
    //Event 1	 : 0-7

    //(1) Event 0 will turn on the digital regulator and start the crystal oscillator
    //(2) Event 1 will start the receiver
    //(3) RX Timeout will check the condition for staying in RX or terminate receiver
    //(4) Qual = 0: Continue receive if sync word has been found
    //(5) Qual = 1: Continue receive if sync word has been found, or if the preamble quality is above threshold (PQT)

    //Setup interface
    InitSPI();

    //Status must be IDLE
    stat = CC_STATUS; if (!IS_IDLE) { IDLE; WAIT_IDLE(t); }

    // Configuring all the WOR related settings
    // Event1 is the elapsed time after Event0 has occured
    // Enable automatic initial calibration of RCosc.
    // Set T_event1 ~ 1.4 ms, enough for XOSC stabilize and FS calibration before RX.
    // Enable RC oscillator before starting with WOR (or else it will not wake up).
    // AUTO_SYNC function disabled.
    RF_Write(WORCTRL, 0x08 | resolution | (event1<<4), &stat);


    // Set Event0 timeout = 1000 ms (RX polling interval)
    // Assuming f_xosc = 26 MHz
    // WOR_RES = 1
    // EVENT0 = 1083
    // T_event0 = (750 / f_xosc) * EVENT0 * 2^(5*WOR_RES)
    // EVENT0 = (T_event0 * f_xosc) / (750 * 2^(5*WOR_RES))
// T_evnet0 = (750/26M)*1083*2^32 = 999.69msec
    RF_Write(WOREVT1, event0>>8, &stat);
    RF_Write(WOREVT0, event0&0xFF, &stat);


    // Setting Rx_timeout
    // Rx_timeout = T_event0 / (2^(8+WOR_RES))
    // Do not terminate if SYNC word has been found or PQI (preamble quality indicator)
    // is set
    RF_Write(MCSM2, (qual<<3) | (term_rssi<<4) | rx_timeout, &stat);


    IDLE;
    WOR_RESET;
    START_WOR;
}


// **************************************************************************
//
//  FUNCTION    :   ConfigGDO2
//
//  IN PARA     :   bool inv, u8 cfg
//
//  OUT PARA    :   void
//
//  OPERATION   :   Configure the GDO2 pin output
//
//  CHANGES     :   2010/07/03 - Create operation
//
// **************************************************************************
void ConfigGDO2(bool inv, u8 cfg)
{
    //Locals
    u8 stat;

    RF_Write(IOCFG2, (inv<<6) | cfg, &stat);
}


// **************************************************************************
//
//  FUNCTION    :   ConfigGDO0
//
//  IN PARA     :   bool inv, u8 cfg
//
//  OUT PARA    :   void
//
//  OPERATION   :   Configure the GDO0 pin output
//
//  CHANGES     :   2010/07/03 - Create operation
//
// **************************************************************************
void ConfigGDO0(bool inv, u8 cfg)
{
    //Locals
    u8 stat;

    RF_Write(IOCFG0, (inv<<6) | cfg, &stat);
}


// **************************************************************************
//
//  FUNCTION    :   TransmitCarrier
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Setup the CC1101 to transmit a carries
//
//  CHANGES     :   2010/12/14 - Create operation
//
// **************************************************************************
void TransmitCarrier()
{
    //Locals
    u8 stat, t;

    //Must initialize SPI before use
    InitSPI();

    //Status must be IDLE
    if (!IS_IDLE) { IDLE; WAIT_IDLE(t); }

    RF_Write(PKTCTRL0, 0x12, &stat);  		//Whitening off, Serial Synchronous mode, Infinite length packets
    RF_Write(DEVIATN,  0x00, &stat);   		//Setup for NO deviation
    RF_Write(MDMCFG2,  0x30, &stat);   		//Setup for ASK/OOK
    RF_Write(PATABLE, TX_POWER, &stat);

    //Do transmit
    ENABLE_TX;
}


// **************************************************************************
//
//  FUNCTION    :   ConfigChannel
//
//  IN PARA     :   u8 chan
//
//  OUT PARA    :   void
//
//  OPERATION   :   Configure the RF Channel
//
//  CHANGES     :   2011/05/27 - Create operation
//
// **************************************************************************
void ConfigChannel(u8 chan)
{
    //Locals
    u8 stat;

    RF_Write(CHANNR, chan, &stat);
}


// **************************************************************************
//
//  FUNCTION    :   InitRF
//
//  IN PARA     :   void
//
//  OUT PARA    :   bool
//
//  OPERATION   :   Initialize the tranceiver
//
//  CHANGES     :   2010/07/03 - Create operation
//
// **************************************************************************
bool InitRF()
{
    u8 t;
    bool result;

    CLRWDT();
    
    //Must initialize SPI before use
    InitSPI();

    //Status must be IDLE
    if (!IS_IDLE) { IDLE; WAIT_IDLE(t); }

    result = RF_Test(TRUE);

    //Set default register settings
    RF_Default();

    RF_SetAddress(0x77);

    //Packet has been received
    ConfigGDO2(FALSE, 0x01);

    ConfigChannel(RF_CHANNEL);

    //Set initial state
    if (!IS_IDLE) { IDLE; WAIT_IDLE(t); }
    FLUSH_RX_FIFO;
    FLUSH_TX_FIFO;
    FREQ_SYNTH_CAL;
    if (!IS_IDLE) { IDLE; WAIT_IDLE(t); }

    //Power down the tranceiver
    SETUP_AND_START_WOR;

    return(result);
}


// **************************************************************************
//
//  FUNCTION    :   RF_SetupHeader
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Setup header for packet
//
//  CHANGES     :   2010/11/21 - Create operation
//
// **************************************************************************
void RF_SetupHeader(union_rf_header *header, u8 family, u8 device, u8 type, u8 flags)
{
    //Locals
    u8 i;

    //Get raw battery voltage
    A2D_BatteryVoltage();

    //Set header
    header->fields.family 	= family;
    header->fields.device 	= device;
    header->fields.type   	= type;
    header->fields.index  	= rf_packet_index++;
    header->fields.battery	= battery_voltage;
    header->fields.hops 	= 0;
    header->fields.version 	= device_version;
    header->fields.flags	= flags;


    //Store my serial number
    for (i=0; i<11; i++) header->fields.adr[i] = device_id[i];
}


// **************************************************************************
//
//  FUNCTION    :   RF_BroadcastMotion
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Broadcast the health message now
//
//  CHANGES     :   2011/08/19 - Create operation
//
// **************************************************************************
void RF_BroadcastMotion()
{
    //Locals
    u8 t;
    union_rf_motion *rf_motion;

    LED_ON;

    //PACKET: | LEN | ADR | FAM | DEV | TYP | IDX | ID[11] | VER | DAT | CRC |
    rf_motion = (union_rf_motion *)(&rf_data[2]);

    //Set header
    RF_SetupHeader((union_rf_header *)rf_motion, TYPE_FAMILY, TYPE_DEVICE, rf_type_accel, 0x00);

    //Set fields
    rf_motion->fields.motion = sensor_data.motion_cnt_send;
    rf_motion->fields.x_max = sensor_data.acc_max_x;
    rf_motion->fields.y_max = sensor_data.acc_max_y;
    rf_motion->fields.z_max = sensor_data.acc_max_z;
    rf_motion->fields.x_min = sensor_data.acc_min_x;
    rf_motion->fields.y_min = sensor_data.acc_min_y;
    rf_motion->fields.z_min = sensor_data.acc_min_z;

    //Send Response
    RF_TX_Packet(rf_motion->all, sizeof(union_rf_motion), PACKET_FILTER, 15);

    //Status must be IDLE
    if (!IS_IDLE) { IDLE; WAIT_IDLE(t); }

    //Tranceiver must go back to sleep
    SETUP_AND_START_WOR;

    LED_OFF;
}

// **************************************************************************
//
//  FUNCTION    :   RF_BroadcastLocation
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Broadcast the location now
//
//  CHANGES     :   2011/08/19 - Create operation
//
// **************************************************************************
void RF_BroadcastLocation()
{
    //Locals
    u8 t;
    union_basic_location_data *rf_loc;

    LED_ON;

    //PACKET: | LEN | ADR | FAM | DEV | TYP | IDX | ID[11] | VER | DAT | CRC |
    rf_loc = (union_basic_location_data *)(&rf_data[2]);

    //Set header
    RF_SetupHeader((union_rf_header *)rf_loc, TYPE_FAMILY, TYPE_DEVICE, rf_type_basic_location, 0x00);

    //Set fields
    rf_loc->fields.day = gps_info.day;
    rf_loc->fields.month = gps_info.month;
    rf_loc->fields.year = gps_info.year;
    rf_loc->fields.hour = gps_info.hour;
    rf_loc->fields.min = gps_info.min;
    rf_loc->fields.sec = gps_info.sec;
    rf_loc->fields.latitude = gps_info.latitude;
    rf_loc->fields.longitude = gps_info.longitude;
    rf_loc->fields.ttf = gps_info.ttf;

    //Send Response
    RF_TX_Packet(rf_loc->all, sizeof(union_basic_location_data), PACKET_FILTER, 1000);

    //Status must be IDLE
    if (!IS_IDLE) { IDLE; WAIT_IDLE(t); }

    //Tranceiver must go back to sleep
    SETUP_AND_START_WOR;

    LED_OFF;
}


// **************************************************************************
//
//  FUNCTION    :   ProcessRF_Events
//
//  IN PARA     :   bool tx_now
//
//  OUT PARA    :   void
//
//  OPERATION   :   Process RF events
//
//  CHANGES     :   2010/11/21 - Create operation
//
// **************************************************************************
void ProcessRF_Events()
{
    //Locals
    union_rf_header   *rf_header;
    union_basic_location_data *rf_loc;
    struct_rf_status  *rf_status;
    rf_device_types rx_device;
    u8 cnt, t;


    //PACKET: | LEN | ADR | FAM | DEV | TYP | IDX | ID[11] | VER | DAT | CRC |
    rf_header = (union_rf_header *)(&rf_data[2]);


    //-----------------------------------------------------------------------
    //SOMETHING RECEIVED?
    //-----------------------------------------------------------------------
    if (IS_GDO2_HI)
    {
        InitSPI();

        //Packet may still be receiving
        WaitMs(20);

        //Get the packet count
        cnt = RF_RxCount();

        //Read data from receiver
        RF_FIFO_Read(rf_data, cnt);

        //Get the status bytes
        rf_status = (struct_rf_status*)(&rf_data[cnt-2]);

        //CRC ok?
        if (rf_status->lqi & 0b10000000)
        {
            //Keep device information
            rx_device = rf_header->fields.device;

            //Packet type switch
            switch (rf_header->fields.type)
            {
                case rf_type_basic_location:
                {
                    #if (HAS_GPS == FALSE)
                    rf_loc = (union_basic_location_data *)rf_header;
                    
                    //Temp
                    //LED_ON; WaitMs(10); LED_OFF; WaitMs(10); LED_ON; WaitMs(10); LED_OFF;

                    gps_info.day = rf_loc->fields.day;
                    gps_info.month = rf_loc->fields.month;
                    gps_info.year = rf_loc->fields.year;
                    gps_info.hour = rf_loc->fields.hour;
                    gps_info.min = rf_loc->fields.min;
                    gps_info.sec = rf_loc->fields.sec;
                    gps_info.latitude = rf_loc->fields.latitude;
                    gps_info.longitude = rf_loc->fields.longitude;
                    gps_info.ttf = rf_loc->fields.ttf;

                    location_rdy = TRUE;

                    #endif

                    break;
                }
                default:
                {

                    break;
                }
            }
        }

        //Status must be IDLE
        if (!IS_IDLE) { IDLE; WAIT_IDLE(t); }

        //Tranceiver must go back to sleep
        FLUSH_RX_FIFO;
        SETUP_AND_START_WOR;
    }
    //-----------------------------------------------------------------------
    //SOMETHING TO TRANSMIT?
    //-----------------------------------------------------------------------
    else if (cpu_state.fields.rf_send_motion)
    {
        InitSPI();
        cpu_state.fields.rf_send_motion = FALSE;

        cpu_timers.gps_send_timer = TIME_TRANSMIT/2;
        
        RF_BroadcastMotion();
        ACC_Update(TRUE);
    }
    else if (cpu_state.fields.rf_send_loc)
    {
        cpu_state.fields.rf_send_loc = FALSE;

        if (location_rdy)
        {
            InitSPI();
            RF_BroadcastLocation();
        }
    }
}


// **************************************************************************
//
//  FUNCTION    :   ProcessRF_Timing
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Process RF timing
//
//  CHANGES     :   2011/02/28 - Create operation
//
// **************************************************************************
void ProcessRF_Timing()
{
    //MOTION TIMER
    if (cpu_timers.motion_timer == EXPIRED)
    {
        cpu_timers.motion_timer = TIME_MEASUREMENT;

        //Max-hold
        if (sensor_data.motion_cnt > sensor_data.motion_cnt_hold) sensor_data.motion_cnt_hold = sensor_data.motion_cnt;

        //Reset motion counter
        sensor_data.motion_cnt = 0;        
    }
    //MOTION TRANSMIT TIMER
    else if (cpu_timers.motion_send_timer == EXPIRED)
    {
        cpu_timers.motion_send_timer = TIME_TRANSMIT;

        //Reset motion-hold counter
        sensor_data.motion_cnt_send = sensor_data.motion_cnt_hold;
        sensor_data.motion_cnt_hold = 0;

        if (!cpu_state.fields.gps_active) cpu_state.fields.rf_send_motion = TRUE;
    }
    else if (cpu_timers.gps_send_timer == EXPIRED)
    {
        cpu_timers.gps_send_timer = TIME_DISABLED;

        if (!cpu_state.fields.gps_active) cpu_state.fields.rf_send_loc = TRUE;
    }
    //HEALTH TIMER
    else if (cpu_timers.tx_health_timer == EXPIRED)
    {
        cpu_timers.tx_health_timer = TEN_MINUTES;

        //Reconfigure 
        InitRF();
        ACC_AsyncMode();
        WaitMs(100);
    }
}


#endif