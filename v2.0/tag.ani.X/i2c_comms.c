// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   tag.ani
//
//      UNIT    :   i2c_comms.c
//
//      AUTHOR  :   Nico Bestbier
//                  Ph : +27 (0)79 976-3335
//                  E-mail : nico@accitrack.co.za
//
// **************************************************************************

#ifndef __I2C_C
#define __I2C_C

#include "header.h"


// **************************************************************************
//
//  FUNCTION    :   InitI2C
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Initialize the I2C for master communications
//
//  CHANGES     :   2013/05/06 - Create operation
//
// **************************************************************************
void InitI2C()
{
    //Shutdown serial port
    SSPCON1bits.SSPEN = 0;

    //Pin configuration
    CC_CSn_HI;                  //Place the CC1101 in HI-Z
    TRISC3 = PORT_INPUT;	//SCL[RC3]
    TRISC4 = PORT_INPUT;        //SDA[RC4]

    //I2C Master Setup
    SSPSTATbits.SMP = 1;
    SSPSTATbits.CKE = 0;

    //Master Clock = FOSC/4
    SSPCON1bits.WCOL  = 0;
    SSPCON1bits.SSPOV = 0;
    SSPCON1bits.SSPM3 = 1;
    SSPCON1bits.SSPM2 = 0;
    SSPCON1bits.SSPM1 = 0;
    SSPCON1bits.SSPM0 = 0;
    SSPCON1bits.CKP   = 1;      //Enable clock

    //Clear control register 2
    SSPCON2 = 0x00;
    SSPCON3 = 0X00;

    SSPCON3bits.SDAHT = 1;

    //Clock period = ((ADD<7:0> + 1)*4)/FOSC ~ 100KHz
    //SSPADD = 19;
    SSPADD = 5;
}


// **************************************************************************
//
//  FUNCTION    :   i2c_write_buff
//
//  IN PARA     :   u8 slave, plugin_reg reg, u8 *data, u16 len
//
//  OUT PARA    :   void
//
//  OPERATION   :   Print a buffer on the I2C interface
//
//  CHANGES     :   2012/08/28 - Create operation
//
// **************************************************************************
void i2c_write_buff(u8 slave, u8 reg, u8 *data, u16 len)
{
    //Locals
    u16 i, t;

    //Init interface
    PIR1bits.SSPIF = 0;
    PIR2bits.BCLIF = 0;
    SSPCON1bits.SSPEN = 1;

    //Set start bit
    I2C_START; I2C_WAIT(t);

    //Set address for write
    I2C_ADDR(slave, FALSE); I2C_WAIT(t);

    //Send register value
    if (!SSPCON2bits.ACKSTAT)
    {
        I2C_REGISTER(reg); I2C_WAIT(t);
    }
    else
    {
        cpu_state.fields.i2c_error = TRUE;
    }

    //Send payload
    if (!SSPCON2bits.ACKSTAT)
    {
        for (i=0; i<len; i++)
        {
            //Write command
            SSPBUF = data[i];
            I2C_WAIT(t);

            //Acknowledge was not received - stop transmitting
            I2C_NOT_ACK_BREAK;
        }
    }
    else
    {
        cpu_state.fields.i2c_error = TRUE;
    }

    //Set stop bit
    I2C_STOP; I2C_WAIT(t);

    SSPCON1bits.SSPEN = 0;
}


// **************************************************************************
//
//  FUNCTION    :   plugin_read_buff
//
//  IN PARA     :   u8 slave
//              :   u8 reg
//              :   u8 *data
//              :   u8 len
//
//  OUT PARA    :   u16
//
//  OPERATION   :   Read data from the I2C interface
//
//  CHANGES     :   2012/08/28 - Create operation
//
// **************************************************************************
u16 i2c_read_buff(u8 slave, u8 reg, u8 *data, u16 len)
{
    //Locals
    u16 i, t;

    //Init interface
    PIR1bits.SSPIF = 0;
    PIR2bits.BCLIF = 0;
    SSPCON1bits.SSPEN = 1;

    //Set start bit and address
    I2C_START; I2C_WAIT(t);
    I2C_ADDR(slave, FALSE); I2C_WAIT(t);

    //Data communication
    if (!SSPCON2bits.ACKSTAT)
    {
        //Send register value
        I2C_REGISTER(reg); I2C_WAIT(t);

        //Get data
        if (!SSPCON2bits.ACKSTAT)
        {
            //Set the repeated start bit
            I2C_REPEATED_START; I2C_WAIT(t);

            //Set address for read
            I2C_ADDR(slave, TRUE); I2C_WAIT(t);

            //Read payload
            if (!SSPCON2bits.ACKSTAT)
            {
                for (i=0; i<len; i++)
                {
                    //Wait for slave to respond
                    I2C_RX_START; I2C_WAIT(t);
                            
                    //Handle data
                    data[i] = SSPBUF;

                    //Send ack. Enable receiver again?
                    if (i < len-1) { I2C_ACK; I2C_WAIT(t); }
                }
            }
            else
            {
                cpu_state.fields.i2c_error = TRUE;
            }
        }
        else
        {
            cpu_state.fields.i2c_error = TRUE;
        }
    }
    else
    {
        cpu_state.fields.i2c_error = TRUE;
    }

    //Set nack and stop bit
    I2C_NACK; I2C_WAIT(t);
    I2C_STOP; I2C_WAIT(t);

    //Disable port
    SSPCON1bits.SSPEN = 0;
    
    return(i);
}

// **************************************************************************
//
//  FUNCTION    :   ACC_AsyncMode
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Setup for async read
//
//  CHANGES     :   2013/10/15 - Create operation
//
// **************************************************************************
void ACC_AsyncMode()
{
    u8 data[1];

    //Initialize the I2C port
    InitI2C();

    //Initialize accelerometer in standby mode
    data[0] = 0b00000000; i2c_write_buff(KXTJ2_ADR, 0x1B, &data, 1);

    //Set low current mode (8-bit resolution), +-4g
    data[0] = 0b00001000; i2c_write_buff(KXTJ2_ADR, 0x1B, &data, 1);

    //Set the output data rate 25Hz
    data[0] = 0b00000001; i2c_write_buff(KXTJ2_ADR, 0x21, &data, 1);

    //Set accelerometer in operating mode
    data[0] = 0b10001000; i2c_write_buff(KXTJ2_ADR, 0x1B, &data, 1);
}

// **************************************************************************
//
//  FUNCTION    :   ACC_AsyncRead
//
//  IN PARA     :   s8* x, s8* y, s8* z
//
//  OUT PARA    :   void
//
//  OPERATION   :   Read the x,y,z parameters
//
//  CHANGES     :   2013/10/15 - Create operation
//
// **************************************************************************
void ACC_AsyncRead(s8* x, s8* y, s8* z)
{
    //Initialize the I2C port
    InitI2C();

    //Get X
    i2c_read_buff(KXTJ2_ADR, 0x07, (u8*)x, 1);
    //Get Y
    i2c_read_buff(KXTJ2_ADR, 0x09, (u8*)y, 1);
    //Get Z
    i2c_read_buff(KXTJ2_ADR, 0x0B, (u8*)z, 1);
}


// **************************************************************************
//
//  FUNCTION    :   ACC_Update
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Process the accelerometer
//
//  CHANGES     :   2013/10/15 - Create operation
//
// **************************************************************************
void ACC_Update(bool reset)
{
    //Get G-values
    s8 x, y, z;
    bool invalid;

    if (!reset)
    {
        //Reset the error state
        cpu_state.fields.i2c_error = FALSE;

        //Get the xyz readings
        ACC_AsyncRead(&x, &y, &z);

        //Anomaly?
        invalid = ((x==0) && (y==0) && (z==0));

        if ((!cpu_state.fields.i2c_error) && (!invalid))
        {
            if (x > sensor_data.acc_max_x) sensor_data.acc_max_x = x;
            if (y > sensor_data.acc_max_y) sensor_data.acc_max_y = y;
            if (z > sensor_data.acc_max_z) sensor_data.acc_max_z = z;

            if (x < sensor_data.acc_min_x) sensor_data.acc_min_x = x;
            if (y < sensor_data.acc_min_y) sensor_data.acc_min_y = y;
            if (z < sensor_data.acc_min_z) sensor_data.acc_min_z = z;

            #if HAS_VIBRATION == FALSE
            if ((x > G_TRIGGER) || (y > G_TRIGGER) || (z > G_TRIGGER)) sensor_data.motion_cnt++;
            else if ((x < -G_TRIGGER) || (y < -G_TRIGGER) || (z < -G_TRIGGER)) sensor_data.motion_cnt++;
            #endif
        }
    }
    else
    {
        sensor_data.acc_max_x = -128;
        sensor_data.acc_max_y = -128;
        sensor_data.acc_max_z = -128;
        sensor_data.acc_min_x = 127;
        sensor_data.acc_min_y = 127;
        sensor_data.acc_min_z = 127;
    }
}

#endif
