// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   AcciTag
//
//      UNIT    :   cpu.h
//
//      AUTHOR  :   Nico Bestbier                  
//                  Ph : +27 (0)82 313-0903
//                  E-mail : bes@xsinet.co.za
//
// **************************************************************************

#ifndef __CPU_H
#define __CPU_H

#ifdef __CPU_C
 #define CPULOCN
#else
 #define CPULOCN extern
#endif


#define EXPIRED         0
#define TIME_DISABLED   -1
#define ONE_SECOND      8L
#define THREE_SECONDS   24L
#define FIVE_SECONDS    39L
#define TEN_SECONDS     78L
#define THIRTY_SECONDS  234L
#define ONE_MINUTE      469L
#define TWO_MINUTES     ONE_MINUTE*2L
#define THREE_MINUTES   ONE_MINUTE*3L
#define FIVE_MINUTES    ONE_MINUTE*5L
#define TEN_MINUTES     ONE_MINUTE*10L
#define TWENTY_MINUTES  ONE_MINUTE*20L
#define THIRTY_MINUTES  ONE_MINUTE*30L
#define ONE_HOUR        ONE_MINUTE*60L
#define TWO_HOURS       ONE_HOUR*2L
#define SIX_HOURS       ONE_HOUR*6L

#define EEPROM_CHANNEL	0

typedef struct
{
    unsigned factory_test_mode	:1;
    unsigned new_cmd_ready  	:1;
    unsigned gps_active		:1;
    unsigned r12		:1;
    unsigned r11		:1;
    unsigned r10		:1;
    unsigned r9			:1;
    unsigned r8			:1;
    unsigned r7			:1;
    unsigned r6			:1;
    unsigned r5			:1;
    unsigned r4			:1;
    unsigned i2c_error          :1;
    unsigned rf_error		:1;
    unsigned rf_send_motion	:1;
    unsigned rf_send_loc	:1;
} struct_cpu_state;


typedef union
{
    struct_cpu_state fields;
    u16 all;
} union_cpu_state;


typedef struct
{
    //Timers
    u16 tx_health_timer;
    u16 motion_timer;
    u16 motion_send_timer;
    s32 gps_interval_timer;
    s32 gps_timeout_timer;
    s32 gps_send_timer;
    
    //Counters
    u16 factory_mode_counter;
} struct_cpu_timers;

typedef struct
{
    u16 motion_cnt;
    u16 motion_cnt_hold;
    u16 motion_cnt_send;
    s8  acc_max_x;
    s8  acc_max_y;
    s8  acc_max_z;
    s8  acc_min_x;
    s8  acc_min_y;
    s8  acc_min_z;
} struct_sensors;

#define LED_ON				(RB6 = 1)
#define LED_OFF				(RB6 = 0)
#define LED_TOG				(RB6 ^= 1)
#define CC_CSn_HI			(RC1 = 1)
#define CC_CSn_LO			(RC1 = 0)
#define IS_GDO2_HI			(RC0)
#define IS_RX_HI			(RC7)
#define PA_ON				(RB1 = 0);(RB5 = 1)
#define PA_OFF				(RB5 = 0)
#define HGM_ON				(RB2 = 1)
#define HGM_OFF				(RB2 = 0)
#define LNA_ON				(RB5 = 0);(RB1 = 1)
#define LNA_OFF				(RB1 = 0)
#define GPS_ON				RA7 = 1; cpu_state.fields.gps_active = TRUE
#define GPS_OFF				RA7 = 0; cpu_state.fields.gps_active = FALSE

CPULOCN u8 battery_voltage;
CPULOCN struct_sensors sensor_data;
CPULOCN void WaitMs(u16 time);
CPULOCN void WaitUs(u16 time);
CPULOCN void InitCpu();
CPULOCN void InitNonVolatile();
CPULOCN void ProcessButtons();
CPULOCN void ProcessTimers();


CPULOCN u8                      scratch[16];
CPULOCN u8                      device_id[12];
CPULOCN union_cpu_state		cpu_state;
CPULOCN struct_cpu_timers	cpu_timers;

#endif