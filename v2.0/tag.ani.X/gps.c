// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   AcciTag
//
//      UNIT    :   gps.c
//
//      AUTHOR  :   Nico Bestbier
//                  Ph : +27 (0)82 313-0903
//                  E-mail : bes@xsinet.co.za
//
// **************************************************************************

#ifndef __GPS_C
#define __GPS_C

#include "header.h"

#define GPSBAUD 	9600
#define FOSC 		8000000L
#define GPSDIVIDER 	((int)(FOSC/(16UL * GPSBAUD) - 1))

#if (HAS_GPS == TRUE)

void InitGPS()
{
    //Baud rate
    SPEN = 0;
    SPBRGL = GPSDIVIDER;			//BAUD rate = FOSC/[16 * (SPBRG + 1)]
    BRG16 = 0;

    //Transmitter setup
    CSRC  = 0;					//Ignore
    TX9   = 0;					//Selects 8-bit transmission
    TXEN  = 1;					//Transmit enabled
    SYNC  = 0;					//Asynchronous mode
    BRGH  = 1;					//High Speed
    TX9D  = 0;					//Ninth bit of Transmit Data

    //Receiver setup
    RCIF  = 0;					//Clear interrupt flag
    RCIE  = 1;					//Enable interrupt
    RX9   = 0;					//Selects 8-bit reception
    SREN  = 0;					//Ignore
    ADDEN = 0;					//Disables address detection
    CREN  = 1;

    SPEN  = 1;					//Serial port enabled

    //Comm Vars
    comms_in_ptr = 0;
    comms_in_len = 0;

    //Enable interrupts (we need interrupts for the UART)
    GIE = 1; PEIE = 1;

    #if GPS_DEBUG == TRUE
    WaitMs(10); printf("\rINIT GPS\r"); WaitMs(10);
    #endif
}


// **************************************************************************
//
//  FUNCTION    :   strtok_
//
//  IN PARA     :   u8* s1, const u8 s2
//
//  OUT PARA    :   u8*
//
//  OPERATION   :   My custom tokenize function
//
//  CHANGES     :   2010/10/31 - Create operation
//
// **************************************************************************
u8* strtok_ptr;
u8* strtok_start;
u8* strtok_(u8* s1, const u8 s2)
{
    //Initialize the pointer
    if (s1 != NULL) strtok_ptr = s1;

    //Keep start of field
    strtok_start = strtok_ptr;

    //Next Field
    while (*strtok_ptr != NULL)
    {
        //NULL terminate
        if (*strtok_ptr == s2)
        {
            *strtok_ptr = 0;
            strtok_ptr++;
            return (strtok_start);
        }

        strtok_ptr++;
    }

    //End of tokenize
    if (*strtok_start != NULL) return (strtok_start);
    else return (NULL);
}


void ProcessGPScmds()
{
    //Locals
    char* c;
    char* t;
    u8 tok = ',';
    u8 index = 0;
    double v;
    bool valid = FALSE;
    
    if (strncmp(comms_buff, "$GPRMC", 6) == 0)
    {
        LED_ON;
        #if GPS_DEBUG == TRUE
        printf("%s\r", comms_buff);
        #endif

        //Start tokenize
        c = strtok_(comms_buff, tok);

        while (c != NULL)
        {
            if (*c != 0)
            {
                switch(index)
                {
                    //TIME
                    case 1:
                    {
                        t = c;
                       
                        break;
                    }
                    //STATUS - "A" is valid
                    case 2:
                    {
                        valid = (*c == 'A');

                        if (valid)
                        {
                            strncpy(scratch, &t[0], 2); scratch[2] = 0;
                            gps_info.hour = atoi(scratch);
                            strncpy(scratch, &t[2], 2); scratch[2] = 0;
                            gps_info.min = atoi(scratch);
                            strncpy(scratch, &t[4], 2); scratch[2] = 0;
                            gps_info.sec = atoi(scratch);
                            
                            //Valid position ready
                            location_rdy = TRUE;

                            //Stop GPS
                            if (cpu_timers.gps_timeout_timer > THREE_SECONDS)
                            {                               
                                gps_info.ttf = (u16)((GPS_TIMEOUT - cpu_timers.gps_timeout_timer)/ONE_SECOND);

                                cpu_timers.gps_timeout_timer = THREE_SECONDS;
                            }
                            
                            //Verbose
                            #if GPS_DEBUG == TRUE
                            printf("TIM: %02d:%02d:%02d\r", gps_info.hour, gps_info.min, gps_info.sec);
                            #endif
                        }

                        break;
                    }
                    //LATITUDE - ddmm.mmmm
                    case 3:
                    {
                        if (valid)
                        {
                            strncpy(scratch, &c[0], 2); scratch[2] = 0;
                            v = atof(scratch);
                            strncpy(scratch, &c[2], 16); scratch[8] = 0;
                            v += (atof(scratch)/(double)60.0);
                            gps_info.latitude = (s32)(v*(double)100000.0);

                            //Verbose
                            #if GPS_DEBUG == TRUE
                            printf("LAT: %ld\r", gps_info.latitude);
                            #endif
                        }

                        break;
                    }
                    //LATITUDE - N/S
                    case 4:
                    {
                        if (valid)
                        {
                            if (*c=='S') gps_info.latitude *= -1;

                            //Verbose
                            #if GPS_DEBUG == TRUE
                            printf("NS : %s\r", c);
                            #endif
                        }

                        break;
                    }
                    //LONGITUDE - dddmm.mmmm
                    case 5:
                    {
                        if (valid)
                        {
                            strncpy(scratch, &c[0], 3); scratch[3] = 0;
                            v = atof(scratch);
                            strncpy(scratch, &c[3], 16); scratch[8] = 0;
                            v += (atof(scratch)/(double)60.0);
                            gps_info.longitude = (s32)(v*(double)100000.0);

                            //Verbose
                            #if GPS_DEBUG == TRUE
                            printf("LON: %ld\r", gps_info.longitude);
                            #endif
                        }

                        break;
                    }
                     //LONGITUDE - E/W
                    case 6:
                    {
                        if (valid)
                        {
                            if (*c=='W') gps_info.longitude *= -1;

                            //Verbose
                            #if GPS_DEBUG == TRUE
                            printf("EW : %s\r", c);
                            #endif
                        }

                        break;
                    }
                    //DATE
                    case 9:
                    {
                        if (valid)
                        {
                            strncpy(scratch, &c[0], 2); scratch[2] = 0;
                            gps_info.day = atoi(scratch);
                            strncpy(scratch, &c[2], 2); scratch[2] = 0;
                            gps_info.month = atoi(scratch);
                            strncpy(scratch, &c[4], 2); scratch[2] = 0;
                            gps_info.year = atoi(scratch);

                             //Verbose
                            #if GPS_DEBUG == TRUE
                            printf("DAT: %02d-%02d-%02d\r", gps_info.day, gps_info.month, gps_info.year);
                            #endif
                        }

                        break;
                    }
                }
            }

            //Next
            c = strtok_(NULL, tok);
            index++;
        }

        LED_OFF;
    }
}

void ProcessGPS()
{
    //Switch on GPS
    if (cpu_timers.gps_interval_timer == EXPIRED)
    {
        cpu_timers.gps_interval_timer = GPS_INTERVAL;
        cpu_timers.gps_timeout_timer = GPS_TIMEOUT;
        
        InitGPS();
        GPS_ON;
    }

    //GPS timeout occured
    if (cpu_timers.gps_timeout_timer == EXPIRED)
    {
        #if GPS_DEBUG == TRUE
        printf("GPS OFF\r", comms_buff);
        WaitMs(10);
        #endif
        
        cpu_timers.gps_timeout_timer = TIME_DISABLED;
        GPS_OFF;
    }
    //GPS Active here
    else if (cpu_timers.gps_timeout_timer > EXPIRED)
    {
        //Check for port errors
        if (RCSTAbits.FERR || RCSTAbits.OERR) { SPEN = 0; SPEN = 1; }

        if (cpu_state.fields.new_cmd_ready)
        {
            ProcessGPScmds();

            cpu_state.fields.new_cmd_ready = FALSE;
        }
    }
}

#endif

#endif

