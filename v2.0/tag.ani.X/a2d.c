// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   AcciTrack
//
//      UNIT    :   a2d.c
//
//      AUTHOR  :   Nico Bestbier                  
//                  Ph : +27 (0)82 313-0903
//                  E-mail : bes@xsinet.co.za
//
// **************************************************************************

#ifndef __A2D_C
#define __A2D_C

#include "header.h"


// **************************************************************************
//
//  FUNCTION    :   InitA2D
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Initialize the A2D module
//
//  CHANGES     :   2010/03/27 - Create operation
//
// **************************************************************************
void InitA2D()
{
	//ADCON0: A/D CONTROL REGISTER 0
	CHS3   = 0;			//Analog Channel Select bits
	CHS2   = 0;
	CHS1   = 1;
	CHS0   = 0;
	ADON   = 0;			//ADC Enable bit

	//ADCON1: A/D CONTROL REGISTER 1
	ADCS2  = 1;			//A/D Conversion Clock Select bits (FOSC/64)
	ADCS1  = 1;
	ADCS0  = 0;
	//ADREF1 = 1;
	//ADREF0 = 1;
	ADPREF1 = 1;
	ADPREF0 = 1;

	//FVRCON: FIXED VOLTAGE REFERENCE REGISTER
//	ADFVR1 = 1;			//A/D Converter Fixed Voltage Reference Peripheral output is 2x (2.048V)
//	ADFVR0 = 0;
//	FVREN  = 1;			//Fixed Voltage Reference Enable bit
}


// **************************************************************************
//
//  FUNCTION    :   A2D_BatteryVoltage
//
//  IN PARA     :   void
//
//  OUT PARA    :   void
//
//  OPERATION   :   Read the battery voltage
//
//  CHANGES     :   2010/03/27 - Create operation
//
// **************************************************************************
void A2D_BatteryVoltage()
{
	ADFVR1 = 1;			//A/D Converter Fixed Voltage Reference Peripheral output is 2x (2.048V)
	ADFVR0 = 0;
	FVREN  = 1;			//Fixed Voltage Reference Enable bit
	//Enable conversion
	ADIF = 0;
	ADON = 1;

	//Acquisition time
	WaitMs(1);
	if (FVRRDY);		//Check if the Fixed Voltage Reference is stable

	//Start conversion
	//GODONE = 1;
	GO_nDONE = 1;

	//Wait for conversion done
	//while(GODONE) continue;
	while(GO_nDONE) continue;

	//Calculate battery voltage
	//battery_voltage = ADRES;
	battery_voltage = ADRESH;

	//Disable A2D converter
	ADON = 0;
	FVREN  = 0;			//Fixed Voltage Reference Disable
	ADFVR1 = 0;			//A/D Converter Fixed Voltage Reference Peripheral output is disabled
	ADFVR0 = 0;

}


#endif