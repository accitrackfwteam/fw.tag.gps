// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   AcciTrack
//
//      UNIT    :   fifo.h
//
//      AUTHOR  :   Nico Bestbier                  
//                  Ph : +27 (0)82 313-0903
//                  E-mail : bes@xsinet.co.za
//
// **************************************************************************

#ifndef __FIFO_H
#define __FIFO_H

#ifdef __FIFO_C
 #define FIFOLOCN
#else
 #define FIFOLOCN extern
#endif

#define STACK_SIZE			20


FIFOLOCN u32 stack_array[STACK_SIZE];
FIFOLOCN u8  stack_time[STACK_SIZE];

FIFOLOCN void stack_process();
FIFOLOCN void InitStack();
FIFOLOCN bool stack_push(u8* snr);
FIFOLOCN bool stack_exists(u8* snr);

#endif