// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   AcciTag
//
//      UNIT    :   config.h
//
//      AUTHOR  :   Nico Bestbier                  
//                  Ph : +27 (0)82 313-0903
//                  E-mail : bes@xsinet.co.za
//
// **************************************************************************

#ifndef __CONFIG_H
#define __CONFIG_H

//SWITCHES
#define HAS_GPS             TRUE
#define GPS_DEBUG           FALSE
#define HAS_VIBRATION       FALSE

//DEFINES
#define TYPE_DEVICE         rf_device_animal_tag
#define TYPE_FAMILY         rf_family_tag

//CONSTANTS
#define G_TRIGGER           64
#define RF_CHANNEL          0

//TIMING
#define TIME_MEASUREMENT    ONE_MINUTE
#define TIME_TRANSMIT       THREE_MINUTES
#define GPS_INTERVAL        THIRTY_MINUTES
#define GPS_TIMEOUT         THREE_MINUTES

//FORMAT:	16-bit value :: MMmm
const u16 device_version = 0x0200;

//TESTING
#define TEST_TAG            TRUE

//TAG.GPS
#define TEST_SRN            "43041610001"

//TAG.ANI
//#define TEST_SRN          "40111310071"


#endif


