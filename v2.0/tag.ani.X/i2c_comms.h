// **************************************************************************
//
//      BESTBIER FAMILY TRUST
//
//      PROJECT :   imet.sense
//
//      UNIT    :   i2c_comms.h
//
//      AUTHOR  :   Nico Bestbier
//                  Ph : +27 (0)79 976-3335
//                  E-mail : nico@accitrack.co.za
//
// **************************************************************************

#ifndef __I2C_H
#define __I2C_H

#ifdef __I2C_C
#define I2CLOCN
#else
#define I2CLOCN extern
#endif


#define KXTJ2_ADR   0b00011100

//I2C MICROS -------------------------------------------------------------------

#define I2C_ACK             SSPCON2bits.ACKDT = 0; SSPCON2bits.ACKEN = 1
#define I2C_NACK            SSPCON2bits.ACKDT = 1; SSPCON2bits.ACKEN = 1
#define I2C_RX_START        SSPCON2bits.RCEN = 1
#define I2C_START           SSPCON2bits.SEN = 1
#define I2C_REPEATED_START  SSPCON2bits.RSEN = 1
#define I2C_ADDR(slave,rd)  SSPBUF = slave + rd
#define I2C_REGISTER(reg)   SSPBUF = reg
#define I2C_STOP            SSPCON2bits.PEN = 1
#define I2C_NOT_ACK_BREAK   if(SSPCON2bits.ACKSTAT) break

#define I2C_WAIT(t)         t = 0; \
                            while(t < 1000) { t++; if (PIR1bits.SSPIF) break; } \
                            PIR1bits.SSPIF = 0                           

//PROTOTYPES -------------------------------------------------------------------

I2CLOCN void InitI2C();
I2CLOCN void ACC_AsyncMode();
I2CLOCN void ACC_AsyncRead(s8* x, s8* y, s8* z);
I2CLOCN void ACC_Update(bool reset);

#endif
